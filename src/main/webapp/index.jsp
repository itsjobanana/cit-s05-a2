<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Job Finder</title>
		<style>
			div{
			margin-top: 5px;
			margin-bottom: 5px;
		}
			button{
				color:blue;
			}
			
		/* #container{
			display:flex;
			flex-direction:column;
			justify-content: center;
			align-items: center;
		} */
		/* div > input, div > select, div > textarea{
			width: 96%; */
		}
		
		</style>
	</head>
	<body>
		<div id="container">
		<h1>Welcome to Servlet Job Finder!</h1>
		<form action="register" method="post">
			<!-- First Name -->
			<div>
				<label for="fname">First Name</label>
				<input type = "text" name = "fname" required/>
			</div>
			<!-- Last Name -->
			<div>
				<label for="lname">Last Name</label>
				<input type = "text" name = "lname" required/>
			</div>
			<!-- Phone Number -->
			<div>
				<label for="phone">Phone</label>
				<input type = "tel" name = "phone" required/>
			</div>
			<!-- Email -->
			<div>
				<label for="email">Email</label>
				<input type = "email" name = "email" required/>
			</div>
			<!-- Application Discovery information -->
			<fieldset>
				<legend>How did you discover app?</legend>
				<!-- Friends -->
				<input type = "radio" id="friends" value="friends" name="discovery" required/>
				<label for = "friends">Friends</label>
				<br>
				<!-- Social Media -->
				<input type = "radio" id="social_media" value="social_media" name="discovery" required/>
				<label for = "social_media">Social Media</label>
				<br>
				<!-- Others -->
				<input type = "radio" id="others" value="others" name="discovery" required/>
				<label for = "others">others</label>
			</fieldset>
			<!-- Date of birth -->
			<div>
				<label for ="birthDate">Date of birth</label>
				<input type="date" name="birthDate" required/>
			</div>
			<!-- Applicant of Employer -->
			<div>
				<label for="type">Are you an employer or applicant?</label>
				<input type="text" name="type" required list="type"/>
				
				<datalist id = "type">
					<option value="Applicant">
					<option value="Employer">
				</datalist>
			</div>
			<!-- Description -->
			<div>
				<label for="description">Profile Description</label>
				<textarea name="description" maxlength="500"></textarea>
			</div>
			<button id="button">Register</button>
		</form>
		</div>
	</body>
</html>